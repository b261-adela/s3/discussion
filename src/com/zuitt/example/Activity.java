package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        try {
            int num = 0;
            System.out.println("Input an integer whose factorial will be computed: ");
            long factorial = 1;
            num = input.nextInt();
            for(int i = 1; i<= num; i++ ){
                factorial *= i;
            }
            System.out.println("The factorial of " + num + " is " + factorial);
        }
        catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }

    }
}
