package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    public static void main (String[] args){

        //Loops
            // are control structures that allow code blocks to be executed multiple times

        // While Loop
            /*
            int x =0;
            while(x < 10){
                System.out.println("Loop Number: " + x);
                x++;
            }
            */

        // Do-While
            // Similar to while loops. However, do-while loops will execute atleast once while loop may not execute at all.
        /*
        int y = 10;
        do {
            System.out.println("Countdown: " + y);
            y--;
        } while (y > 0);
        */

        //For loop
        // Syntax:
        /* for(initialValue; condition; iteration){
                //code block
            } */

        /*
        for (int i = 0; i < 10; i++){
            System.out.println("Current count: " + i);
        }
        */

        //For loop with arrays
        /*
        int[] intArray = new int[] {100,200,300,400,500};

        for (int i = 0; i < intArray.length; i++){
            System.out.println(i);
            System.out.println(intArray[i]);
        }
        */

        //For-each loop with array
        // Syntax:
            /*
            for(dataType itemName: arrayName){
                //code block
            }
            */
        /*
        String[] nameArray = new String[] {"John", "Paul", "George", "Ringo"};
        for (String name: nameArray){
            System.out.println(name);
        }
        */

        // Nested for Loops
        /*String[][] classroom = new String[3][3];
        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";
        //Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";*/

        /* nested for loop multidimensional
        for (int row = 0; row < 3; row ++){
            for (int col = 0; col < 3; col ++){
                System.out.println("ClassRoom[" + row + "][" + col + "]" +classroom[row][col]);
            }
        }
        */

        // for-each multidimensional
        //access each row
        /*for(String[] row: classroom){
            //accessing each column
            for(String column : row){
                System.out.println(column);
            }
        }*/

        //for-each with ArrayList
        /* Syntax:
            arrayListName.forEach(consumer -> // code block)
        */

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        System.out.println("Arraylist: " + numbers);

        //"->" this is called lambda operator which is used to separate parameter and implementation
        // -> ~ single arrow
        numbers.forEach(number -> System.out.println("ArrayList: " + number));

        //forEach with HashMaps
        /*Syntax:
            hashMapName.forEach((key, value) -> code block)
        */

        HashMap<String, Integer> grades= new HashMap<>(){{
            put("English", 90);
            put("Math", 95);
            put("Science", 97);
            put("History", 94);
        }};

        grades.forEach((subject, grade) -> System.out.println(subject + ": " +grade));
    }
}
